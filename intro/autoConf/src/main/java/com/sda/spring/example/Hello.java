package com.sda.spring.example;

import com.sda.spring.example.beans.FirstName;
import com.sda.spring.example.interfaces.BeanName;
import com.sda.spring.example.beans.MyBean;
import com.sda.spring.example.beans.SecondName;
import com.sda.spring.example.config.AppConfig;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Hello {

    public static void main(String[] args){

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(AppConfig.class);
        context.refresh();

        // 4 more dependencies : CONS
        BeanName myStartBeanName = new FirstName( );
        MyBean myStartBean = new MyBean(myStartBeanName );

        // PROS:
        MyBean bean = context.getBean( MyBean.class );
        System.out.println(bean.sayHello());

        bean.setName(new SecondName());
        System.out.println(bean.sayHello());
    }
}
