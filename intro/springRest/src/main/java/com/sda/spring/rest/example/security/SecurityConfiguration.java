package com.sda.spring.rest.example.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;

// REF: https://www.javadevjournal.com/spring/what-is-spring-security/

@Configuration
@Slf4j
class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        // Custom encoder
        SCryptPasswordEncoder passwordEncoder = new SCryptPasswordEncoder(
                16, 8, 4, 128, 96
        );
        return passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        httpSecurity
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers(new String[]{"/users", "/h2"})
                .permitAll()
                .anyRequest().authenticated()
                .and()
                .httpBasic();

    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception {

        PasswordEncoder encoder = new SCryptPasswordEncoder(

        );

        log.info( "Using password encoder: {}", encoder );

        // Do not disclose the password in code! 
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password( "$40804$N9760DKlQUM/Oub/g/7zSytwl0A17D0D90wVwpwTog4gQGxDelT+Q+lc9KqUbY5q3fF7r6o+xvNgWOcuNUPAEOvhLr3lv3RjAT91/iLkt9x9y9VTTY1O/wJnrC3/WkA/$30swaJHygQPYbDGxjAvB46KV3W2V5Axlf2DPE4pn2xsCvK1pW04y6Q/8ENuy27Vu/QpabCsTgqw9md4foJoIjnIBe3MNMSHlL8jHuuq2BnPgVatsvUFYEy9ULFagpH8gWy89hD+1ei+foN/o1EuX4p7y8ihyYVaJTu32sSSzAPA=")
                .roles("USER");

        log.info( "In memory password is like: {}", encoder.encode("passw0rd") );
    }

}