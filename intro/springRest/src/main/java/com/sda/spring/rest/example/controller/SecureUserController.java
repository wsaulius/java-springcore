package com.sda.spring.rest.example.controller;

import com.sda.spring.rest.example.entities.User;
import com.sda.spring.rest.example.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

// REF: https://www.baeldung.com/spring-security-basic-authentication

@RestController
public class SecureUserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users")   //   localhost:8080/api/users/
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

}
