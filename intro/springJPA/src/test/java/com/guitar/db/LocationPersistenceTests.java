package com.guitar.db;

import com.guitar.db.model.Location;
import com.guitar.db.repository.LocationJpaRepository;
import com.guitar.db.repository.LocationRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
@TestPropertySource(locations = "classpath:/application-test.properties")
@SpringBootTest(classes = {LocationJpaRepository.class, LocationRepository.class})
@Slf4j
public class LocationPersistenceTests extends PersistenceTestBase {

    @Autowired
    private LocationJpaRepository locationJpaRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    public void testJpaFindAll() {

        List<Location> locations = locationJpaRepository.findAll();
        log.debug("LOCATIONS count: {}", locations.size());

        assertNotNull(locations);
        assertFalse(locations.isEmpty());
    }

    @Test
    @Transactional
    public void testSaveAndGetAndDelete() throws Exception {

        AtomicReference<Optional<Location>> location =
                new AtomicReference<>(Optional.ofNullable(new Location()));

        // Should check for the duplicate
        assertDoesNotThrow(() -> {

            location.get().get().setCountry("Canada");
            location.get().get().setState("British Columbia");
            location.set(Optional.of(locationRepository.create(location.get().get())));

        }, "All well with the create check.");

        // clear the persistence context so we don't return the previously cached location object
        // this is a test only thing and normally doesn't need to be done in prod code
        entityManager.clear();

        Location otherLocation = locationRepository.find(location.get().get().getId());
        assertEquals("Canada", otherLocation.getCountry());
        assertEquals("British Columbia", otherLocation.getState());

        //delete BC location now
        locationRepository.delete(otherLocation);
    }

    @Test
    public void testFindWithLike() throws Exception {
        List<Location> locs = locationRepository.getLocationByStateName("New");
        assertEquals(4, locs.size());
    }

    @Test
    @Transactional  //note this is needed because we will get a lazy load exception unless we are in a tx
    public void testFindWithChildren() throws Exception {

        final Optional<Location> arizona = Optional.ofNullable(locationRepository.find(3L));
        assertEquals("United States", arizona.get().getCountry());
        assertEquals("Arizona", arizona.get().getState());

        assertEquals(1, arizona.get().getManufacturers().size());

        assertEquals("Fender Musical Instruments Corporation",
                arizona.get().getManufacturers().get(0).getName());
    }
}
