package com.guitar.db;

import com.guitar.db.model.Manufacturer;
import com.guitar.db.repository.ManufacturerRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
@TestPropertySource(locations = "classpath:/application-test.properties")
@SpringBootTest(classes = {ManufacturerRepository.class})
@Slf4j
public class ManufacturerPersistenceTests extends PersistenceTestBase {

    @Autowired
    private ManufacturerRepository manufacturerRepository;

    @Test
    public void testGetManufacturersFoundedBeforeDate() throws Exception {
        List<Manufacturer> mans = manufacturerRepository.getManufacturersFoundedBeforeDate(new Date());
        assertEquals(2, mans.size());
    }

    @Test
    public void testGetManufactureByName() throws Exception {
        Manufacturer m = manufacturerRepository.getManufacturerByName("Fender");
        assertEquals("Fender Musical Instruments Corporation", m.getName());
    }

    @Test
    public void testGetManufacturersThatSellModelsOfType() throws Exception {
        List<Manufacturer> mans = manufacturerRepository.getManufacturersThatSellModelsOfType("Semi-Hollow Body Electric");
        assertEquals(1, mans.size());
    }
}
