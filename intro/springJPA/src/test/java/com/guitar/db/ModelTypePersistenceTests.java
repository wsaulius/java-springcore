package com.guitar.db;

import com.guitar.db.model.ModelType;
import com.guitar.db.repository.ModelRepository;
import com.guitar.db.repository.ModelTypeJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
@TestPropertySource(locations = "classpath:/application-test.properties")
@SpringBootTest(classes = {ModelTypeJpaRepository.class})
@Slf4j

public class ModelTypePersistenceTests extends PersistenceTestBase {

    @Autowired
    private ModelTypeJpaRepository modelTypeJpaRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    @Transactional
    public void testSaveAndGetAndDelete() throws Exception {
        ModelType mt = new ModelType();
        mt.setName("Test Model Type");
        mt = modelTypeJpaRepository.save(mt);

        // clear the persistence context so we don't return the previously cached location object
        // this is a test only thing and normally doesn't need to be done in prod code
        entityManager.clear();

        Optional<ModelType> optionalModelType = modelTypeJpaRepository.findById(mt.getId());
        ModelType otherModelType = optionalModelType.get();
        assertEquals("Test Model Type", otherModelType.getName());

        modelTypeJpaRepository.delete(otherModelType);
    }

    @Test
    public void testFind() throws Exception {
        Optional<ModelType> optionalModelType = modelTypeJpaRepository.findById(1L);
        ModelType mt = optionalModelType.get();
        assertEquals("Dreadnought Acoustic", mt.getName());
    }
}
