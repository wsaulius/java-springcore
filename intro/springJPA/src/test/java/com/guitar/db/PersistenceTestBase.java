package com.guitar.db;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.jdbc.Sql;

@EnableAutoConfiguration
@ActiveProfiles("test")
@Sql(scripts = "classpath:/import.sql")
@Slf4j
public class PersistenceTestBase {

    @BeforeAll
    static public void initialize() {

        log.info( "Initialize and START tests");
    }

    @DynamicPropertySource
    static void deferSQL(DynamicPropertyRegistry registry) {

        registry.add("spring.jpa.defer-datasource-initialization",
                () -> "true");

        registry.add("spring.datasource.initialization-mode",
                () -> "always");
    }

}
