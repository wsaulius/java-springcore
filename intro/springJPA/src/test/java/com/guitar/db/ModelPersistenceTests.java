package com.guitar.db;

import com.guitar.db.model.Model;
import com.guitar.db.model.ModelType;
import com.guitar.db.repository.ModelRepository;
import com.guitar.db.repository.ModelTypeJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Example;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
@TestPropertySource(locations = "classpath:/application-test.properties")
@SpringBootTest(classes = {ModelRepository.class})
@Slf4j

public class ModelPersistenceTests extends PersistenceTestBase {

    @Autowired
    ModelTypeJpaRepository modelTypeJpaRepository;

    @Autowired
    private ModelRepository modelRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    @Transactional
    public void testSaveAndGetAndDelete() throws Exception {

        AtomicReference<Model> m = new AtomicReference<>(new Model());
        assertDoesNotThrow( () -> {

                    m.get().setFrets(10);
                    m.get().setName("Test Model");
                    m.get().setPrice(BigDecimal.valueOf(55L));
                    m.get().setWoodType("Maple");
                    m.get().setYearFirstMade(new Date());
                    m.set(modelRepository.create(m.get()));

                }
        );

        // clear the persistence context so we don't return the previously cached location object
        // this is a test only thing and normally doesn't need to be done in prod code
        entityManager.clear();

        Model otherModel = modelRepository.find(m.get().getId());
        assertFalse(otherModel.getName().isEmpty());
        assertEquals(10, otherModel.getFrets());

        //delete BC location now
        modelRepository.delete(otherModel);
    }

    @Test
    public void testGetModelsInPriceRange() throws Exception {
        List<Model> mods = modelRepository.getModelsInPriceRange(BigDecimal.valueOf(1000L), BigDecimal.valueOf(2000L));
        assertEquals(4, mods.size());
    }

    @Test
    public void testGetModelsByPriceRangeAndWoodType() throws Exception {
        List<Model> mods = modelRepository.getModelsByPriceRangeAndWoodType(BigDecimal.valueOf(1000L), BigDecimal.valueOf(2000L), "Maple");
        assertEquals(3, mods.size());
    }

    @Test
    public void testGetModelsByType() throws Exception {

        final ModelType modelType = new ModelType();
        modelType.setName("Electric");

        Example<ModelType> example = Example.of(modelType);
        Optional<ModelType> actual = modelTypeJpaRepository.findOne(example);
        assertTrue(actual.isPresent());

        log.info("Electric MODEL: {}", actual.get());
        assertEquals(4, actual.get().getModels().size());

    }
}
