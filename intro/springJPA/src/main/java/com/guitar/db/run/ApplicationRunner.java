package com.guitar.db.run;

import com.guitar.db.repository.LocationJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ApplicationRunner implements CommandLineRunner {

    @Autowired
    private LocationJpaRepository locationJpaRepository;

    @Override
    public void run(String... args) throws Exception {

        log.info( "START executing in " + this.getClass() );
    }
}
