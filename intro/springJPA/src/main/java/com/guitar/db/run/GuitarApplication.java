package com.guitar.db.run;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication

@ComponentScan(basePackages={"com.guitar.db"})
@EnableJpaRepositories(basePackages="com.guitar.db.repository")
public class GuitarApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuitarApplication.class, args);
    }
}

