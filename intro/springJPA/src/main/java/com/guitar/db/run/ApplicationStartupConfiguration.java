package com.guitar.db.run;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.jdbc.DatabaseDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.support.DatabaseStartupValidator;

import javax.sql.DataSource;

@Configuration
@Slf4j
public class ApplicationStartupConfiguration {

    @Bean
    public DatabaseStartupValidator databaseStartupValidator(DataSource dataSource) {
        final DatabaseStartupValidator dsv = new DatabaseStartupValidator();
        dsv.setDataSource(dataSource);
        dsv.setTimeout(60);
        dsv.setInterval(7);
        dsv.setValidationQuery(DatabaseDriver.H2.getValidationQuery());

        log.info( "WAIT for DB startup");
        return dsv;
    }

}
