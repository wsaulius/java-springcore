package com.guitar.db.model;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import javax.persistence.*;

@Entity
@Table(name = "Location")
public class Location {


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private String state;
	private String country;

	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="LOCATION_ID")
	private List<Manufacturer> manufacturers = new ArrayList<Manufacturer>();

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<Manufacturer> getManufacturers() {
		return manufacturers;
	}

	public void setManufacturers(List<Manufacturer> manufacturers) {
		this.manufacturers = manufacturers;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", Location.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.add("state='" + state + "'")
				.add("country='" + country + "'")
				.add("manufacturers=" + manufacturers)
				.toString();
	}
}
