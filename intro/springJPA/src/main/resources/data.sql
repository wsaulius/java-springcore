SHOW TABLES;

create table location (id bigint not null, country varchar(255), state varchar(255), primary key (id));

create table manufacturer (id bigint not null, average_yearly_sales decimal(19,2), founded_date timestamp, name varchar(255), headquarters_id bigint, location_id bigint, primary key (id));

create table model (id bigint not null, frets integer not null, name varchar(255), price decimal(19,2), wood_type varchar(255), year_first_made timestamp, manufacturer_id bigint, model_type_id bigint, modeltype_id bigint, primary key (id));

create table model_type (id bigint not null, name varchar(255), primary key (id));

alter table manufacturer add constraint FKksvvct8b6uhjpqg9iq4x18hf4 foreign key (headquarters_id) references location;

alter table manufacturer add constraint FK25y5aw7rd1a67vh1tm3d3f35y foreign key (location_id) references location;

alter table model add constraint FK3q3dd1gats5au027tbdy4upfi foreign key (manufacturer_id) references manufacturer;

alter table model add constraint FK4predchy67lxyol7lv15spd6d foreign key (model_type_id) references model_type;

alter table model add constraint FK50gb3mga5uaci6mqpo2nn3q8w foreign key (modeltype_id) references model_type;

SELECT * FROM INFORMATION_SCHEMA.TABLES;


