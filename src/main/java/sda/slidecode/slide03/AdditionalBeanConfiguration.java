package sda.slidecode.slide03;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public class AdditionalBeanConfiguration {

    @Bean // this method injects a dependent bean necessary to create an ObjectMapper object
    public ObjectMapper objectMapper(

            // TODO: Remove or add this only line
            // @Qualifier("dateFormatProvider")
            final IDateFormatProvider provider) {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(provider.get());

        System.out.println( "Constructing mapper from " + provider.get() );
        return objectMapper;
    }

}
