package sda.slidecode.slide03;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import java.text.DateFormat;

@Component("dateFormatProvider")
public class DateFormatProvider implements IDateFormatProvider {

    public DateFormat get() {

        System.out.println( "getting from " + this.getClass().getCanonicalName() );
        return DateFormat.getDateInstance();
    }
}
