package sda.slidecode.slide03;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import sda.slidecode.slide02.SimpleLogger;

import java.time.Instant;
import java.util.Date;

@Component
@Primary
@Slf4j
public class HelloWorldLoggerSlide implements CommandLineRunner {

    @Qualifier("simpleLogger")
    private final SimpleLogger simpleLogger;

    @Autowired
    ObjectMapper objectMapper;

    public HelloWorldLoggerSlide(final SimpleLogger simpleLogger) {
        this.simpleLogger = simpleLogger;
        System.out.println( "Logger type is :" + simpleLogger.getClass().getCanonicalName() );
    }

    @Override
    public void run(final String... args) throws Exception {
        simpleLogger.printMessage("Hello from command line runner");

        log.info( "Format dates as: {}",
                objectMapper.getDateFormat() );

        log.info( "Result: {}", objectMapper.getDateFormat().format( new Date() ));
    }
}