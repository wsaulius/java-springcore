package sda.slidecode.slide03;

import java.text.DateFormat;

public interface IDateFormatProvider {

    public DateFormat get();
}
