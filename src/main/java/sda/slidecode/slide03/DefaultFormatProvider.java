package sda.slidecode.slide03;

import com.fasterxml.jackson.databind.util.StdDateFormat;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.text.DateFormat;

@Component
@Primary
public class DefaultFormatProvider implements IDateFormatProvider {

    @Override
    public DateFormat get() {

        System.out.println( "getting from " + this.getClass().getCanonicalName() );
        System.out.println( "Date as: " + StdDateFormat.DATE_FORMAT_STR_ISO8601 );

        return new StdDateFormat();
    }
}
