package sda.slidecode.slide03;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import sda.slidecode.slide02.SimpleLogger;

@Configuration
@Slf4j
public class SimpleLoggersConfiguration {

    @Bean
    public SimpleLogger verySimpleLogger() {
        return System.out::println;
    }

    @Bean
     @Primary
    public SimpleLogger lombokLogger() {
        System.out.println( "Check for runtime type. Because of primary" );
        return log::info;
    }
}