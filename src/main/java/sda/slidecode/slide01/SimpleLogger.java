package sda.slidecode.slide01;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component( value ="simple_logger" )
@Slf4j
public class SimpleLogger implements IDummyLogger {

    @Override
    public void sayHello() {

        log.info( "hello from {} into SCREEN", this.getClass().getSimpleName() );
        log.debug("hello from {} FILE", this.getClass().getSimpleName() );

    }
}
