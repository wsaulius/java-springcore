package sda.slidecode.slide01;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component( value ="dummy_logger" )
@Slf4j
public class DummyLogger implements IDummyLogger {

    @Override
    public void sayHello() {

        log.info("hello from DummyLogger into SCREEN");
        log.debug("hello from DummyLogger to FILE");
    }
}