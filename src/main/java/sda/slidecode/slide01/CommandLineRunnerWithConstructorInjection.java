package sda.slidecode.slide01;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component( value = "constructed")
@Slf4j
public class CommandLineRunnerWithConstructorInjection implements CommandLineRunner {

    private final IDummyLogger dummyLogger;

    @Override
    public void run(final String... args) throws Exception {
        dummyLogger.sayHello();
    }

    // Inject constructor
    @Autowired
    CommandLineRunnerWithConstructorInjection( @Qualifier("simple_logger") final IDummyLogger dummyLogger ) {

        this.dummyLogger = dummyLogger;
        log.info( "Dummy logger: " + this.dummyLogger );
    }

    // Inject constructor; autowired like "preferred"
    CommandLineRunnerWithConstructorInjection( final DummyLogger dummyLogger ) {

        this.dummyLogger = dummyLogger;
        log.info( "Dummy logger: " + this.dummyLogger );
    }

    @Override
    public String toString() {
        return "CommandLineRunnerWithConstructorInjection{" +
                "dummyLogger=" + dummyLogger +
                '}';
    }
}