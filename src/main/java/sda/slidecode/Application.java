package sda.slidecode;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import sda.exercises.Solutions;
import sda.slidecode.slide01.DummyLogger;
import sda.slidecode.slide01.IDummyLogger;
import sda.slidecode.slide01.SimpleLogger;
import sda.slidecode.slide02.HelloWorldLogger;

@Slf4j
@SpringBootApplication
public class Application {

    @Autowired
    HelloWorldLogger helloWorldLogger2;

    @Autowired
    CommandLineRunner helloWorldLogger3;

    @Autowired
    Solutions solutions;

    public static void main(String[] args) {

        try {
            SpringApplication.run(sda.slidecode.Application.class, args);
        } catch ( Throwable throwable ) {

            throwable.printStackTrace();
        }
    }

    // Move run to separate method
    public void run(String... args) {

        System.out.println( "START!" );

        AnnotationConfigApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class);

        try {
            solutions.run(args);
        } catch (Exception e ) {

            e.printStackTrace();
        }

        // Slide case 01
        try {
            IDummyLogger constructed = appContext.getBean(DummyLogger.class);
            log.info(constructed.toString());

            constructed = (DummyLogger) appContext.getBean("dummy_logger");
            log.info(constructed.toString());

            constructed = appContext.getBean(SimpleLogger.class);
            log.info(constructed.toString());

            constructed = (SimpleLogger) appContext.getBean("simple_logger");
            log.info(constructed.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Slide case 02
        try {
            helloWorldLogger2.run( args );

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Slide case 03
        try {

            helloWorldLogger3.run( args );

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}