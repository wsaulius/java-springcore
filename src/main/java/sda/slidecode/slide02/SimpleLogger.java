package sda.slidecode.slide02;

public interface SimpleLogger {

    void printMessage(String message);
}
