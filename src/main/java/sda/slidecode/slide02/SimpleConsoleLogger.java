package sda.slidecode.slide02;

import org.springframework.stereotype.Component;
import sda.slidecode.slide02.SimpleLogger;

@Component("simpleConsoleLogger")
public class SimpleConsoleLogger implements SimpleLogger {

    @Override
    public void printMessage(final String message) {
        System.out.println("Hello from component that implements interface");
    }
}