package sda.slidecode.slide02;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component("simpleLombokLogger")
public class SimpleLombokLogger implements SimpleLogger {

    @Override
    public void printMessage(final String message) {

        log.info("Hello from Lombok Logger: " + message);
    }
}