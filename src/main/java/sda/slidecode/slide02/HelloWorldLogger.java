package sda.slidecode.slide02;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class HelloWorldLogger implements CommandLineRunner {

    private final SimpleLogger simpleLogger;

    // injecting through a constructor using an interface
    @Autowired
    public HelloWorldLogger(@Qualifier("simpleLombokLogger")
                            final SimpleLogger simpleLogger) {
        this.simpleLogger = simpleLogger;
    }

    public HelloWorldLogger(@Qualifier("simpleConsoleLogger")
                            final SimpleConsoleLogger simpleLogger) {
        this.simpleLogger = simpleLogger;
    }

    @Override
    public void run(final String... args) throws Exception {
        simpleLogger.printMessage("Hello from command line runner");
    }
}
