package sda.slidecode;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan( {"sda.slidecode", "sda.exercises"} )
public class AppConfig {

}
