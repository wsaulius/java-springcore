package sda.springcore.spring_annotations.service;

import sda.springcore.spring_annotations.model.Customer;
import sda.springcore.spring_annotations.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.springcore.spring_annotations.repository.InMemoryRepositoryImpl;

import java.util.List;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
         System.out.println("Using constructor autowiring injection!");
         this.customerRepository = customerRepository;
     }

    @Autowired // - The change is here ! Autowired setter - read about it.
    public void setCustomerRepository(InMemoryRepositoryImpl customerRepository) {
        System.out.println("Using setter autowiring injection!");
        this.customerRepository = customerRepository;
    }

    @Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}
}
