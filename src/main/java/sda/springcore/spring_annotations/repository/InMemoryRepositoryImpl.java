package sda.springcore.spring_annotations.repository;

import org.springframework.stereotype.Repository;
import sda.springcore.spring_annotations.model.Customer;

import java.util.ArrayList;
import java.util.List;

@Repository("inMemoryRepository")
public class InMemoryRepositoryImpl implements CustomerRepository {

    @Override
    public List<Customer> findAll() {
        List<Customer> customers = new ArrayList<>();

        Customer customer = new Customer();

        customer.setFirstname("Petras");
        customer.setLastname("Petraitis from " + this.getClass());

        customers.add(customer);

        return customers;
    }
}
