package sda.springcore.spring_annotations.repository;

import org.springframework.stereotype.Repository;
import sda.springcore.spring_annotations.model.Customer;

import java.util.List;

@Repository
public interface CustomerRepository {
	List<Customer> findAll();
}