package sda.springcore.dependencyinjection;

public interface Repository {

    void save( String name );
}
