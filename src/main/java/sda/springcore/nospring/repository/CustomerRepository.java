package sda.springcore.nospring.repository;

import java.util.List;

import org.springframework.stereotype.Repository;
import sda.springcore.nospring.model.Customer;

@Repository
public interface CustomerRepository {
	List<Customer> findAll();
}