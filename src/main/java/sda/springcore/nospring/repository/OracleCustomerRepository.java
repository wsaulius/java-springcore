package sda.springcore.nospring.repository;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import sda.springcore.nospring.model.Customer;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

@Component
@Repository
public class OracleCustomerRepository implements CustomerRepository {

    @Override
    public List<Customer> findAll() {

        // Note the different Collection here
        List<Customer> customers = new java.util.Vector<Customer>();

        Customer customer = new Customer();

        customer.setFirstname("Petras");
        customer.setLastname("Petraitis");

        customers.add(customer);

        return customers;
    }
}
