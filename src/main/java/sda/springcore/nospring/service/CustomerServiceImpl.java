package sda.springcore.nospring.service;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import sda.springcore.nospring.model.Customer;
import sda.springcore.nospring.repository.CustomerRepository;
import sda.springcore.nospring.repository.HibernateCustomerRepositoryImpl;
import sda.springcore.nospring.repository.OracleCustomerRepository;

@Component
public class CustomerServiceImpl implements CustomerService {

	private final String vr = "0.01";

	// Home made DI, let's do it in Spring instead (later)
	private CustomerRepository customerRepository = ( vr.equals( "0.01" )) ?
			new OracleCustomerRepository() :
	 new HibernateCustomerRepositoryImpl();

	@Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}
}