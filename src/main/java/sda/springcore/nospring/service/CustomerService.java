package sda.springcore.nospring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import sda.springcore.nospring.model.Customer;

@Service
public interface CustomerService {
	List<Customer> findAll();
}