package sda.springcore.spring_xml.service;

import sda.springcore.spring_xml.model.Customer;
import sda.springcore.spring_xml.repository.CustomerRepository;

import java.util.List;

public class CustomerServiceImpl implements CustomerService {

    public CustomerServiceImpl() {
    };

    // setter injection
	 private CustomerRepository customerRepository;

    // Default: byType
     public void setCustomerRepository(CustomerRepository customerRepository) {
         this.customerRepository = customerRepository;
     }

    //  autowire by name
     public void sethCustRep(CustomerRepository customerRepository) {
         this.customerRepository = customerRepository;
     }

    // constructor injection and autowire by constructor
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}
}
