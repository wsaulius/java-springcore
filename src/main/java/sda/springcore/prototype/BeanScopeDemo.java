package sda.springcore.prototype;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class BeanScopeDemo {

    private static Integer classInitCount = 0;
    private String message;

    private List<String> myList;
    private Set<String> mySet;
    private Map<Integer,String> myMap;
    private Properties myProp;

    public void setMessage(String message) throws Exception {
        this.message = message;
        myList.add( message );
    }

    public void getMessage() {

        if ( message == null ) {
            System.out.println("Message is not initialized yet! " + message);
        } else
            System.out.println("Your Message : " + message);

    }

    // To be found in XML :: scope="prototype" init-method="init"
    private void init() {
        System.out.println("Initializing [" + ++classInitCount + "] for " + this.getClass() );
    }

    public List<String> getMyList() {
        return myList;
    }

    public void setMyList(List<String> myList) {
        this.myList = myList;
    }

    public Set<String> getMySet() {
        return mySet;
    }

    public void setMySet(Set<String> mySet) {
        this.mySet = mySet;
    }

    public Map<Integer, String> getMyMap() {
        return myMap;
    }

    public void setMyMap(Map<Integer, String> myMap) {
        this.myMap = myMap;
    }

    public Properties getMyProp() {
        return myProp;
    }

    public void setMyProp(Properties myProp) {
        this.myProp = myProp;
    }

}

