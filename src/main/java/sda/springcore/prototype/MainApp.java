package sda.springcore.prototype;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

import java.util.List;
import java.util.function.Consumer;

/**
 * Demonstrate Spring bean scopes
 * <p>
 * https://www.baeldung.com/spring-bean-scopes
 */
public class MainApp {

    public static void main(String[] args) {

        try {

            ApplicationContext context = new
                    ClassPathXmlApplicationContext("applicationContext-beans.xml");

            BeanScopeDemo objA = (BeanScopeDemo) context.getBean("fillCollection");

            objA.setMessage("I'm once initialized by A");
            objA.getMessage();

            objA.getMyList().forEach( System.out::println );

            BeanScopeDemo objB = (BeanScopeDemo) context.getBean("fillCollection");
            objB.getMessage();

            ExpressionParser parser = new SpelExpressionParser();

            System.out.println(objB.getMyList().get(0));
            objB.getMyList().forEach( System.out::println );
            objB.getMySet().stream().forEach( System.out::println );
            objB.getMyMap().keySet().stream().forEach( System.out::println );

            SimpleEvaluationContext simpleEvaluationContext =
                    SimpleEvaluationContext.forReadWriteDataBinding().build();
            objB.getMyProp().values().stream().forEach( spel -> {

                        Expression cExpr =
                                parser.parseExpression( (String) spel);

                        try {

                            List asList = (List) cExpr.getValue(simpleEvaluationContext);
                            asList.forEach( System.out::println );

                        } catch ( ClassCastException e ) {
                            System.out.println((String) cExpr.getValue(simpleEvaluationContext));
                        }
                    });

            /**
             * A bean with prototype scope will return a *different* instance every time it is requested from the container.
             *
             * Note that the message is not initialized
             * *//*

            BeanScopeDemo objC = (BeanScopeDemo) context.getBean("prototype");
            // objC.getMessage();

            BeanScopeDemo objD = (BeanScopeDemo) context.getBean("singleton");

            objD.setMessage("I'm once initialized by D");
            objD.getMessage();
*/

        } catch (Exception e) {

            e.printStackTrace();

        }


    }
}

