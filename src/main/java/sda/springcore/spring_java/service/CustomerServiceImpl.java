package sda.springcore.spring_java.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import sda.springcore.spring_java.model.Customer;
import sda.springcore.spring_java.repository.CustomerRepository;

import java.util.List;

// property files
@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

    @Autowired
	@Qualifier("customerRepository")
	private CustomerRepository customerRepository;

    @Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}
}
