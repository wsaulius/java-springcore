package sda.springcore.spring_java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import sda.springcore.spring_java.repository.CustomerRepository;
import sda.springcore.spring_java.repository.HibernateCustomerRepositoryImpl;
import sda.springcore.spring_java.repository.InMemoryRepositoryImpl;

@Configuration
@ComponentScan({"sda.springcore.spring_java"})
@PropertySource("app.properties")
public class AppConfig {

/*     @Bean(name = "customerRepository")
     public CustomerRepository getCustomerRepository(){
         return new HibernateCustomerRepositoryImpl();
     }*/

    /***
     *  Alternatively, if you change
     */

    @Bean(name = "customerRepository")
    public CustomerRepository getCustomerRepository(){
        return new InMemoryRepositoryImpl();
    }

}
