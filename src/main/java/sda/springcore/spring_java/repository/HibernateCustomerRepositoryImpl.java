package sda.springcore.spring_java.repository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import sda.springcore.spring_java.model.Customer;

import java.util.ArrayList;
import java.util.List;

@Repository("customerRepository")
public class HibernateCustomerRepositoryImpl implements CustomerRepository {

	// SpEL (SPELL)
    @Value("${dbUsername}")
    private String dbUsername;   // dbUsername = ${XDG_SESSION_DESKTOP}

	@Override
	public List<Customer> findAll() {
		List<Customer> customers = new ArrayList<>();
		
		Customer customer = new Customer();
		
		customer.setFirstname("Jonas");
        customer.setLastname((dbUsername == null || dbUsername.equals("")) ? "Jonaitis" : dbUsername);
		
		customers.add(customer);
		
		return customers;
	}
}
