package sda.springcore.spring_java.repository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import sda.springcore.spring_java.model.Customer;
import sda.springcore.spring_java.repository.CustomerRepository;

import java.util.ArrayList;
import java.util.List;

@Component
public class InMemoryRepositoryImpl implements CustomerRepository {

    @Value("default_user")
    private String dbUsername; // Like dbUsername = "default_user";

    @Override
    public List<Customer> findAll() {
        List<Customer> customers = new ArrayList<>();

        Customer customer = new Customer();

        customer.setFirstname("Petras");
        customer.setLastname((dbUsername == null || dbUsername.equals("")) ? "Petraitis" : dbUsername);

        customers.add(customer);

        return customers;
    }
}
