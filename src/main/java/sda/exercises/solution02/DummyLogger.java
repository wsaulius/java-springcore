package sda.exercises.solution02;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component("sol2logger")
@Slf4j
public class DummyLogger {
    public void sayHello() {
        log.info("hello from DummyLogger: " + this.getClass() );
    }
}
