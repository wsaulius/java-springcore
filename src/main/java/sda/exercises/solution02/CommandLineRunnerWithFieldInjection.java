package sda.exercises.solution02;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CommandLineRunnerWithFieldInjection implements CommandLineRunner {

    @Autowired
    private sda.exercises.solution02.DummyLogger dummyLogger;

    @Override
    public void run(final String... args) throws Exception {
        dummyLogger.sayHello();
    }

    @Override
    public String toString() {
        return "CommandLineRunnerWithFieldInjection{" +
                "dummyLogger=" + dummyLogger +
                '}';
    }
}