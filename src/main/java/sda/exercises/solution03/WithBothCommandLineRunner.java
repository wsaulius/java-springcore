package sda.exercises.solution03;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import sda.slidecode.slide01.IDummyLogger;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Component
@Slf4j
public class WithBothCommandLineRunner implements CommandLineRunner {

    private final List<IDummyLogger> loggers;

    public WithBothCommandLineRunner(final List<IDummyLogger> loggers) {
        this.loggers = loggers;
    }

    @Override
    public void run(final String... args) throws Exception {

        log.info( "Start iterating in " + this.getClass() );
        log.info( "Wait for {} dummy loggers", loggers.size() );

        final AtomicReference<Integer> count = new AtomicReference<>(1);
        loggers.stream().forEach( logger -> {

            System.out.println( count.getAndSet(count.get() + 1) + ") "
                    + logger.getClass() );

            logger.sayHello();

        } );
    }
}
