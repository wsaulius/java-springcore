package sda.exercises.solution03;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import sda.slidecode.slide01.IDummyLogger;

@Component
@RequiredArgsConstructor
public class WithPrimaryCommandLineRunner implements CommandLineRunner {

    @Autowired
    private final IDummyLogger dummyLogger;

    @Override
    public void run(final String... args) throws Exception {
        dummyLogger.sayHello();
    }
}