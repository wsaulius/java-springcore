package sda.exercises.solution03;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import sda.slidecode.slide01.IDummyLogger;

@Component
@Slf4j
@Primary
public class FirstDummyLogger implements IDummyLogger {

    @Override
    public void sayHello() {

        log.info("Hello from " + this.getClass() + " which is marked as Primary");

    }
}