package sda.exercises;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import sda.exercises.solution01.DummyLogger;
import sda.exercises.solution02.CommandLineRunnerWithConstructorInjection;
import sda.exercises.solution02.CommandLineRunnerWithFieldInjection;
import sda.exercises.solution03.FirstDummyLogger;
import sda.exercises.solution03.SecondDummyLogger;
import sda.exercises.solution03.WithBothCommandLineRunner;
import sda.exercises.solution03.WithPrimaryCommandLineRunner;
import sda.slidecode.AppConfig;
import sda.slidecode.slide01.IDummyLogger;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class Solutions {

    // Move run to separate method
    public void run(String... args) {

        log.info( "Executing... " + this.getClass().getSimpleName() );
        AnnotationConfigApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class);

        // Exercise 01 solution
        try {
            CommandLineRunner constructed = appContext.getBean(DummyLogger.class);
            System.out.println( constructed );
            log.info(constructed.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Exercise 02 solution
        try {
            CommandLineRunner constructed = (CommandLineRunner)
                    appContext.getBean("withConstructorDI");
            System.out.println( constructed );
            log.info(constructed.toString());

            constructed = (CommandLineRunnerWithFieldInjection)
                    appContext.getBean(CommandLineRunnerWithFieldInjection.class);
            System.out.println( constructed );
            log.info(constructed.toString());

            constructed = (CommandLineRunnerWithConstructorInjection)
                    appContext.getBean(CommandLineRunnerWithConstructorInjection.class);

            System.out.println( constructed );
            log.info(constructed.toString());

        } catch (Exception e) {

            e.printStackTrace();
        }

        // Exercise 03 solution

        try {

            List<IDummyLogger> injectList = new ArrayList<>();

            IDummyLogger dummy1 = appContext.getBean( FirstDummyLogger.class );
            IDummyLogger dummy2 = appContext.getBean( SecondDummyLogger.class );

            System.out.println( dummy1 );
            System.out.println( dummy2 );

            injectList.add( dummy1 );
            injectList.add( dummy2 );

            // Pass into constructed bean
            CommandLineRunner constructed =
                    appContext.getBean( WithBothCommandLineRunner.class, injectList );

            System.out.println( constructed );
            constructed.run( args );
            constructed = appContext.getBean( WithPrimaryCommandLineRunner.class );

            System.out.println( constructed );
            constructed.run( args );

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}