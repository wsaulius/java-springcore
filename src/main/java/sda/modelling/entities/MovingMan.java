package sda.modelling.entities;

@ModelMan
@NamedMan(manName = "", manAddress = "")
public interface MovingMan {

    public void run( long time );
    public void jump( int cm );
}
