package sda.modelling.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import sda.modelling.entities.Ball;
import sda.modelling.entities.SportsMan;
import sda.modelling.objects.*;

public class SpringMatch {

    public static void main(String[] args) {

        ApplicationContext appCtx = new ClassPathXmlApplicationContext("applicationContext-football.xml");
        SportsMan johnny = appCtx.getBean("johnny", Footballer.class );

        johnny.run( 75 );
        johnny.jump( 30 );

        johnny.shoot(new Football() {
            @Override
            public long move(long meters) {
                System.out.println( "Out!" );
                // Note, we do change the defaulted print "Ball moves" for now
                return 0;
            }
        });

        // Ball ball = new Football(); // Same as : wired ("wiring")
        Ball ball = appCtx.getBean("football", Football.class );

        johnny.shoot( ball );

        NationalTeamPlayer goetze = appCtx.getBean("goetze", NationalTeamPlayer.class );
        NationalTeamTshirt shirt = appCtx.getBean("nteam", NationalTeamTshirt.class );

        goetze.jump( 56 );
        goetze.run( 120 );

        System.out.println( goetze.wears( shirt ));
        Football football = appCtx.getBean("football", Football.class );

        goetze.shoot( football );
        goetze.score( 4 );

        shirt = appCtx.getBean("dortmund", NationalTeamTshirt.class );

        System.out.println( shirt.toString() );
        System.out.println( goetze.wears( shirt ));
    }
}
