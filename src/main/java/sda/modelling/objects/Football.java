package sda.modelling.objects;

import org.springframework.stereotype.Component;
import sda.modelling.entities.Ball;

@Component
public class Football implements Ball {

    @Override
    public long move(long meters) {

        System.out.printf("Ball moves " + meters + " m. ");
        return meters;
    }

}
