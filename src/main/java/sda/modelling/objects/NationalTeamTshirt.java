package sda.modelling.objects;

import org.springframework.stereotype.Component;

@Component
public class NationalTeamTshirt extends TShirt {

    public NationalTeamTshirt( final String logo ) {

        System.out.println( "TShirt logo Passed: " + logo );
        super.logo = logo;
    }

    @Override
    public String displays() {
        return this.logo;
    }

    public void setLogo(String logo) {
        super.logo = logo;
    }

    @Override
    public String toString() {
        return this.displays();
    }
}
