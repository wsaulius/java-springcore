package com.sda.javaremote.demo.service;

import com.sda.javaremote.demo.model.Customer;

import java.util.List;

public interface CustomerService {
	List<Customer> findAll();
}