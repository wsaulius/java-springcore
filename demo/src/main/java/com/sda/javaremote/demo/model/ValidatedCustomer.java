package com.sda.javaremote.demo.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Size;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@PropertySource("classpath:application.properties")
@ConfigurationProperties("app")
@Validated
@Component("validated")
public class ValidatedCustomer {

    @Size(max=11, min=2)
    @Pattern(regexp = "^[A-Z]{1}[a-z]+$")
    private String firstName;

    @NotEmpty
    @Size(max=20, min=2)
    @Pattern(regexp = "^[A-Z]{1}[a-z]+$")
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
