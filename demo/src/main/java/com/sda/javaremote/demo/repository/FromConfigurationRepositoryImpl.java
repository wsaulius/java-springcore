package com.sda.javaremote.demo.repository;

import com.sda.javaremote.demo.model.Customer;
import com.sda.javaremote.demo.model.ValidatedCustomer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("loadFromConfig")
public class FromConfigurationRepositoryImpl implements CustomerRepository {

    @Autowired
    ValidatedCustomer validated;

    @Override
    public List<Customer> findAll() {
        List<Customer> customers = new ArrayList<>();

        Customer customer = new Customer();

        customer.setFirstName( validated.getFirstName() );
        customer.setLastName( validated.getLastName() + " from " + this.getClass());

        customers.add(customer);

        return customers;
    }

}
