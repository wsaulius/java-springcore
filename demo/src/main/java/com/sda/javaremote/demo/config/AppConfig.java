package com.sda.javaremote.demo.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.sda.javaremote.demo")

public class AppConfig {

}
