package com.sda.javaremote.demo.repository;

import com.sda.javaremote.demo.model.Customer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("customerRepository")
public class HibernateCustomerRepositoryImpl implements CustomerRepository {

	@Override
	public List<Customer> findAll() {
		List<Customer> customers = new ArrayList<>();
		
		Customer customer = new Customer();
		
		customer.setFirstName("Jonas");
		customer.setLastName("Jonaitis from " + this.getClass());
		
		customers.add(customer);
		
		return customers;
	}
}
