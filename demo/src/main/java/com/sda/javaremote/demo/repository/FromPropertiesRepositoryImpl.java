package com.sda.javaremote.demo.repository;

import com.sda.javaremote.demo.model.Customer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("fromPropertiesRepository")
@PropertySource("classpath:application.properties")
public class FromPropertiesRepositoryImpl implements CustomerRepository {

    @Value("${firstName}")
    private String firstName;

    @Value("${lastName}")
    private String lastName;

    @Value("Saulius")
    private String myName;

    @Override
    public List<Customer> findAll() {
        List<Customer> customers = new ArrayList<>();

        Customer customer = new Customer();

        customer.setFirstName( firstName );
        customer.setLastName( lastName + " from " + this.getClass());

        customers.add(customer);

        return customers;
    }
}
