package com.sda.javaremote.demo;

import com.sda.javaremote.demo.config.AppConfig;
import com.sda.javaremote.demo.repository.CustomerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

@Slf4j
@SpringBootApplication
public class Application implements CommandLineRunner {

	@Autowired
	// Kind of "defaulted"
	CustomerRepository customerRepository;

	@Autowired
	@Qualifier("inMemoryRepository")
	CustomerRepository hibernateRepository;

	@Autowired
	CustomerRepository fromPropertiesRepository;

	@Autowired
	@Qualifier("loadFromConfig")
	CustomerRepository fromConfigurationRepository;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	public void run(String... args) {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(AppConfig.class);
		context.refresh();

		System.out.println( customerRepository.getClass().getCanonicalName() );
		log.info( "INIT: {}", customerRepository.getClass().getCanonicalName() );
		log.info( "GET: {}", customerRepository.findAll() );

		System.out.println( hibernateRepository.getClass().getCanonicalName() );
		log.info( "INIT: {}", hibernateRepository.getClass().getCanonicalName() );
		log.info( "GET: {}", hibernateRepository.findAll() );

		System.out.println( fromPropertiesRepository.getClass().getCanonicalName() );
		log.info( "INIT: {}", fromPropertiesRepository.getClass().getCanonicalName() );
		log.info( "GET: {}", fromPropertiesRepository.findAll() );

		System.out.println( fromConfigurationRepository.getClass().getCanonicalName() );
		log.info( "INIT: {}", fromConfigurationRepository.getClass().getCanonicalName() );
		log.info( "GET: {}", fromConfigurationRepository.findAll() );

	}

}
