package com.sda.javaremote.demo.repository;

import com.sda.javaremote.demo.model.Customer;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository {
	List<Customer> findAll();
}