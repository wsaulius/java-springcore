package com.sda.javaremote.demo.service;

import com.sda.javaremote.demo.model.Customer;
import com.sda.javaremote.demo.repository.CustomerRepository;
import com.sda.javaremote.demo.repository.InMemoryRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
         System.out.println("Using constructor autowiring injection!");
         this.customerRepository = customerRepository;
     }

    @Autowired // - The change is here ! Autowired setter - read about it.
    public void setCustomerRepository(InMemoryRepositoryImpl customerRepository) {
        System.out.println("Using setter autowiring injection!");
        this.customerRepository = customerRepository;
    }

    @Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}
}
