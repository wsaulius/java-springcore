package com.sda.javaremote.demo.repository;

import com.sda.javaremote.demo.model.Customer;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("inMemoryRepository")
//@Primary
public class InMemoryRepositoryImpl implements CustomerRepository {

    @Override
    public List<Customer> findAll() {
        List<Customer> customers = new ArrayList<>();

        Customer customer = new Customer();

        customer.setFirstName("Petras");
        customer.setLastName("Petraitis from " + this.getClass());

        customers.add(customer);

        return customers;
    }
}
