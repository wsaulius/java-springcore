package com.sda.javaremote.demo.model;


// That's what we call a POJO: Plain Old Java Object, in some implementations "a bean"
// A building lego brick in Spring none less

public class Customer {

	protected String firstName;
	protected String lastName;
	
	public Customer () { }

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Customer{");

		sb.append("firstname='").append(firstName).append('\'');
		sb.append(", lastname='").append(lastName).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
